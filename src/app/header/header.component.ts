import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit {
  constructor() {}
  headerConfig = {
    brandLogo: "./assets/brand_logo.png",
    brandName: "./assets/brand_name.png",
  };
  navBarOpen = true;
  todayDate = new Date();
  ngOnInit() {}

  navBarAction() {
    this.navBarOpen = !this.navBarOpen;
  }
}
