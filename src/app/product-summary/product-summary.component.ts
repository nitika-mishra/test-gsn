import { Component, OnInit } from "@angular/core";
import { Route } from "@angular/compiler/src/core";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-product-summary",
  templateUrl: "./product-summary.component.html",
  styleUrls: ["./product-summary.component.css"],
})
export class ProductSummaryComponent implements OnInit {
  productDetails = [
    {
      key: "productCode",
      icon: "./assets/filter.png",
      name: "Product Code",
    },
    {
      key: "productName",
      icon: "./assets/filter.png",
      name: "Product Name",
    },
    {
      key: "productLine",
      icon: "./assets/filter.png",
      name: "Product Line",
    },
    {
      key: "productScale",
      icon: "./assets/filter.png",
      name: "Product Scale",
    },
    {
      key: "productDescription ",
      name: "Product Description",
      icon: "./assets/filter.png",
    },
  ];

  productData = [
    {
      id: 1,
      data: [
        {
          id: 1,
          productCode: {
            name: "AS123",
          },
        },
        {
          id: 2,
          productName: {
            name: "XYZ Helo",
          },
        },
        {
          id: 3,
          productLine: {
            name: "Line",
          },
        },
        {
          id: 4,
          productScale: {
            name: "Scale",
          },
        },
        {
          id: 5,
          productDescription: {
            name: "Description",
          },
        },
      ],
    },
    {
      id: 2,
      data: [
        {
          id: 1,
          productCode: {
            name: "AS123",
          },
        },
        {
          id: 2,
          productName: {
            name: "XYZ Helo",
          },
        },
        {
          id: 3,
          productLine: {
            name: "Line",
          },
        },
        {
          id: 4,
          productScale: {
            name: "Scale",
          },
        },
        {
          id: 5,
          productDescription: {
            name: "Description",
          },
        },
      ],
    },
    {
      id: 3,
      data: [
        {
          id: 1,
          productCode: {
            name: "AS123",
          },
        },
        {
          id: 2,
          productName: {
            name: "XYZ Helo",
          },
        },
        {
          id: 3,
          productLine: {
            name: "Line",
          },
        },
        {
          id: 4,
          productScale: {
            name: "Scale",
          },
        },
        {
          id: 5,
          productDescription: {
            name: "Description",
          },
        },
      ],
    },
    {
      id: 4,
      data: [
        {
          id: 1,
          productCode: {
            name: "AS123",
          },
        },
        {
          id: 2,
          productName: {
            name: "XYZ Helo",
          },
        },
        {
          id: 3,
          productLine: {
            name: "Line",
          },
        },
        {
          id: 4,
          productScale: {
            name: "Scale",
          },
        },
        {
          id: 5,
          productDescription: {
            name: "Description",
          },
        },
      ],
    },
    {
      id: 5,
      data: [
        {
          id: 1,
          productCode: {
            name: "AS123",
          },
        },
        {
          id: 2,
          productName: {
            name: "XYZ Helo",
          },
        },
        {
          id: 3,
          productLine: {
            name: "Line",
          },
        },
        {
          id: 4,
          productScale: {
            name: "Scale",
          },
        },
        {
          id: 5,
          productDescription: {
            name: "Description",
          },
        },
      ],
    },
    {
      id: 6,
      data: [
        {
          id: 1,
          productCode: {
            name: "AS123",
          },
        },
        {
          id: 2,
          productName: {
            name: "XYZ Helo",
          },
        },
        {
          id: 3,
          productLine: {
            name: "Line",
          },
        },
        {
          id: 4,
          productScale: {
            name: "Scale",
          },
        },
        {
          id: 5,
          productDescription: {
            name: "Description",
          },
        },
      ],
    },
    {
      id: 7,
      data: [
        {
          id: 1,
          productCode: {
            name: "AS123",
          },
        },
        {
          id: 2,
          productName: {
            name: "XYZ Helo",
          },
        },
        {
          id: 3,
          productLine: {
            name: "Line",
          },
        },
        {
          id: 4,
          productScale: {
            name: "Scale",
          },
        },
        {
          id: 5,
          productDescription: {
            name: "Description",
          },
        },
      ],
    },
    {
      id: 8,
      data: [
        {
          id: 1,
          productCode: {
            name: "AS123",
          },
        },
        {
          id: 2,
          productName: {
            name: "XYZ Helo",
          },
        },
        {
          id: 3,
          productLine: {
            name: "Line",
          },
        },
        {
          id: 4,
          productScale: {
            name: "Scale",
          },
        },
        {
          id: 5,
          productDescription: {
            name: "Description",
          },
        },
      ],
    },
    {
      id: 9,
      data: [
        {
          id: 1,
          productCode: {
            name: "AS123",
          },
        },
        {
          id: 2,
          productName: {
            name: "XYZ Helo",
          },
        },
        {
          id: 3,
          productLine: {
            name: "Line",
          },
        },
        {
          id: 4,
          productScale: {
            name: "Scale",
          },
        },
        {
          id: 5,
          productDescription: {
            name: "Description",
          },
        },
      ],
    },
  ];
  config: any;
  showProductInfo = false;
  paginationData = { count: 9, data: [] };
  constructor(private router: Router, private route: ActivatedRoute) {
    this.paginationData.data = this.productData;
    this.config = {
      itemsPerPage: 4,
      currentPage: 1,
      totalItems: this.paginationData.count,
    };
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }

  ngOnInit() {}

  showProductDetails(data) {
    localStorage.setItem("product", JSON.stringify(data));
  }
}
