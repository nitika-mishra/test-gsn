import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { SideNavComponent } from "./side-nav/side-nav.component";
import { ProductSummaryComponent } from "./product-summary/product-summary.component";
import { ProductDetailComponent } from "./product-detail/product-detail.component";
import { OrderDetailComponent } from "./order-detail/order-detail.component";
import { AppRoutingModule } from "./app-routing.module";
import { ClickOutsideDirective } from "./header/click-outside.directive";
import { NgxPaginationModule } from "ngx-pagination";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SideNavComponent,
    ProductSummaryComponent,
    ProductDetailComponent,
    OrderDetailComponent,
    ClickOutsideDirective,
  ],
  imports: [BrowserModule, AppRoutingModule, NgxPaginationModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
