import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProductSummaryComponent } from "./product-summary/product-summary.component";
import { RouterModule, Routes } from "@angular/router";
import { ProductDetailComponent } from "./product-detail/product-detail.component";

const routes: Routes = [
  {
    path: "product",
    component: ProductSummaryComponent,
  },
  {
    path: "product/details/:productId",
    component: ProductDetailComponent,
  },
  // {
  //   path: "customer",
  //   component: "",
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
