import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-product-detail",
  templateUrl: "./product-detail.component.html",

  styleUrls: ["./product-detail.component.css"],
})
export class ProductDetailComponent implements OnInit {
  productId: any;
  productData: any;
  sortData = {
    orderNumber: "orderNumber",
    orderDate: "orderDate",
    requiredDate: "requiredDate",
    shippedDate: "shippedDate",
    status: "status",
    comment: "comment",
  };
  orderDetails = [
    {
      name: "Order Number",
      icon: "./assets/filter.png",
    },
    {
      name: "Order Date",
      icon: "./assets/filter.png",
    },
    {
      name: "Required Date",
      icon: "./assets/filter.png",
    },
    {
      name: "Shipped Date",
      icon: "./assets/filter.png",
    },
    {
      name: "Status ",
      icon: "./assets/filter.png",
    },
    {
      name: "Comments ",
      icon: "./assets/filter.png",
    },
  ];

  orderData = [
    {
      id: 1,
      data: [
        {
          id: 1,
          key: "orderNumber",
          name: "Order Number",
        },
        {
          id: 2,
          key: "orderDate",
          name: "Order Date",
        },
        {
          id: 3,
          key: "requiredDate",
          name: "Required Date",
        },
        {
          id: 4,
          key: "shippedDate",
          name: "Shipped date",
        },
        {
          id: 5,
          key: "status",
          name: "Status",
        },
        {
          id: 6,
          key: "comment",
          name: "status",
        },
      ],
    },
    {
      id: 2,
      data: [
        {
          id: 1,
          key: "orderNumber",
          name: "Order Number",
        },
        {
          id: 2,
          key: "orderDate",
          name: "Order Date",
        },
        {
          id: 3,
          key: "requiredDate",
          name: "Required Date",
        },
        {
          id: 4,
          key: "shippedDate",
          name: "Shipped date",
        },
        {
          id: 5,
          key: "status",
          name: "Status",
        },
        {
          id: 6,
          key: "comment",
          name: "status",
        },
      ],
    },
    {
      id: 3,
      data: [
        {
          id: 1,
          key: "orderNumber",
          name: "Order Number",
        },
        {
          id: 2,
          key: "orderDate",
          name: "Order Date",
        },
        {
          id: 3,
          key: "requiredDate",
          name: "Required Date",
        },
        {
          id: 4,
          key: "shippedDate",
          name: "Shipped date",
        },
        {
          id: 5,
          key: "status",
          name: "Status",
        },
        {
          id: 6,
          key: "comment",
          name: "status",
        },
      ],
    },
  ];
  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.productId = this.route.snapshot.paramMap.get("productId");
    this.productData = JSON.parse(localStorage.getItem("product")).data;

    console.log(this.productData);
  }
}
